import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncrementerPage } from './incrementer';

@NgModule({
  declarations: [
    IncrementerPage,
  ],
  imports: [
    IonicPageModule.forChild(IncrementerPage),
  ],
})
export class IncrementerPageModule {}
