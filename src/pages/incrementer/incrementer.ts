import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the IncrementerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-incrementer',
  templateUrl: 'incrementer.html',
})
export class IncrementerPage {
  // @Input() value: string; // decorate the property with @Input()

  intovalue = 0;
  @Input('value') set setValue(value: number) { this.intovalue = value; };

  intostepper = 0;
  @Input('steppervalue') set setStep(value: number) { this.intostepper = value; }

  minvalue = 0;
  @Input('minvalue') set setMinValue(value:number){ this.minvalue = value;}

  maxvalue = 0;
  @Input('maxvalue') set setMaxValue(value:number) {this.maxvalue =value;}

  @Output('valueChange') valueChange: EventEmitter<number> = new EventEmitter();

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  step(control: number) {
    if (control == 1) {
      this.intovalue += this.intostepper;
    } else {
      this.intovalue -= this.intostepper;
    }
    this.checkValue();
    this.addValueChange(this.intovalue);
  }

  addValueChange(value: any) {
    this.valueChange.emit(value);
  }

  checkValue(){
    if(this.intovalue < this.minvalue){
      alert("HO NO YOU BUY LOW");
      this.intovalue = this.minvalue;
    } else if(this.intovalue > this.maxvalue) {
      alert("PLASE BUY DUPLICATE");
      this.intovalue = this.maxvalue;
    }
  }

  ionViewDidLoad() { console.log('ionViewDidLoad IncrementerPage'); }

  showMe() {
    this.addValueChange(this.intovalue);
    console.log("DA DA DA DA ~");
  }
}
